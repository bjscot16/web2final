<?php $this->extend('/Photo-Synthesis/wrapper'); ?>

<?php $this->section('content'); ?>

		<?php
			$imageArray = array();
			foreach ($userUploadData as $row) {
		?>
			<div class="postDiv postDivLeft" xmlns="">
				<img alt="User Image" src="<?php echo 'GetImage/image/' . $row['file_name'] ?>">
				<h4><?php echo $row['pic_title'] ?></h4>
				<h5><?php echo $row['username'] ?></h5>
				<p><?php echo $row['pic_description'] ?></p>
			</div>
		<?php
			}
		?>
	</div>

<?php $this->endSection();
