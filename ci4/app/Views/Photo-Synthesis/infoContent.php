<?php $this->extend('/Photo-Synthesis/wrapper'); ?>

<?php $this->section('content'); ?>

	<div id="content-div">
		<h1>Welcome to Photo-Synthesis</h1>
		<h2>What is Photo-Synthesis?</h2>
			<p>Photo-Synthesis is a place where you can upload photos to share with others! You must
			create an account to upload photos to your personal page. However if you would like to
			check out what other people have posted all you need to do is check out the photo gallery
			page. That's where all the photos that have been marked as public appear. The newest post
			appear at the top while older uploads are present below.</p>
		<h2>Getting Started</h2>
			<p>To get started here on Photo-Synthesis all you need to do is click on the login
			link at the top of the page. There you will have the option of creating an account. All
			accounts are free. Once you have logged in you will be directed to your personal uploads
			page where you can view all of your uploaded content and make modifications to the the photo
			public status. After your first time logging in you will be presented with all of the
			relevant information for getting started. Additionally that information is provided on
			this page as well.</p>
		<h2>Uploading Photos</h2>
			<p>Once you have created your account you are able to get started immediately! Simply click
			on the upload button to select the image you would like to share. Then, type in a title for
			the photo as well as a description. This description can be anything that you would like.
			We do ask that you keep it relevant to the picture and appropriate. Otherwise it will
			be taken down and your account will be deleted. After filling out the description you will
			Once you are done filling in the fields you will have the option to make the photo public or
			keep it private to your own account for you to view. will be able to click submit and the
			photo will be added! Note: The title and description are
			mandatory but they can be as simple as you like.</p>
	</div>

<?php $this->endSection();
