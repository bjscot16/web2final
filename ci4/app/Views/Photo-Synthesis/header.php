<!doctype html>

<html lang="en">
	<head>
  		<meta charset="utf-8">
	  	<title><?= $title ?></title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link id="mainStyleLink" rel="stylesheet" type="text/css" href="<?php echo base_url('style/mainStyleFinal.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= $styleHref ?>">
	</head>
	<body class="row">
		<header>
			<div class="container col-12">
				<img id="logoImage" alt="Logo" src="/images/logo.png">
				<nav>
					<ul>
						<?php
							foreach ((array) $links as $key => $value) {
								echo  "<li> <a href='$value'>" . $key . "</a></li>";
							}
						?>
					</ul>
				</nav>
			</div>
		</header>
		<div id="asideLeft" class="col-2"></div>
		<div id="mainDiv" class="col-8">
