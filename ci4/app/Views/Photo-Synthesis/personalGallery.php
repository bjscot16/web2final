<?php $this->extend('/Photo-Synthesis/wrapper'); ?>

<?php $this->section('content'); ?>
		<div class="row">
			<div class="col-3"></div>
			<div class="card col-6">
				<h3 class="card-header">Post a New Photo</h3>
			<?php echo form_open_multipart('PersonalGallery/addPhoto'); ?>
				<div class="card-body">
					<div class="form-group form-label-group">
						<label for="picTitle">Picture Title:</label>
						<input type="text" id="picTitle" name="picTitle">
					</div>
					<div class="form-group form-label-group">
						<label for="picDescription">Description:</label>
						<input type="text-box" id="picDescription" name="picDescription">
					</div>
					<div class="form-group">
						<input type="file" name="file">
					</div>
					<div class="form-group form-label-group">
						<label for="publicFlag">Check the box to make your photo public:</label>
						<input type="checkbox" id="publicFlag" name="publicFlag" value="Y">
					</div>
					<p>Both a title and image are required to successfully make a post</p>
					<button type="submit" class="btn btn-primary">Post Photo</button>
				</div>
			<?php echo form_close(); ?>
			</div>
			<div class="col-3"></div>
		</div>
		<?php
			foreach ($userUploadData as $row) {
		?>
			<div class="postDiv postDivLeft">
				<img alt="User Image" src="<?php echo 'GetImage/image/' . $row['file_name'] ?>">
				<h4><?php echo $row['pic_title'] ?></h4>
				<h5><?php echo $row['username'] ?></h5>
				<p><?php echo $row['pic_description'] ?></p>
			</div>
		<?php
			}
		?>
	</div>

<?php $this->endSection();
