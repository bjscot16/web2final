<?php namespace App\Controllers;


class GetImage extends BaseController {

	/*
	 * Summary:
	 * 		The GetImage image method uses the $fileName parameter to select the correct image
	 * 		to be returned. Each file name has been designed to be unique so the query will only
	 * 		return one row. The image location is then assigned to $image location and the mime type
	 * 		of the image is queried from the database. The image is retrieved and then the response
	 * 		header is set and returned
	 * @param: $fileName contains the image file name that is being requested
	 * @return: requested image
	 */
	public function image($fileName) {

		// Establish database connection to 'user_posts' table
		$builder = $this->db->table('user_posts');

		// Retrieve the information related to the image from the user's post
		$queryResults = $builder->select('*')->where('file_name', $fileName)->get();
		$postInfoArray = $queryResults->getResult('array');

		// Create variables for image location and mime type
		$imageLocation = WRITEPATH . 'uploads/userImages/' . $fileName;
		$imageMimeType = $postInfoArray[0]['mime_type'];

		$image = file_get_contents($imageLocation);

		return $this->response->setHeader('Content-Type', $imageMimeType)
							->setStatusCode(200)
							->setBody($image);
	}

}
