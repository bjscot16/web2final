<?php namespace App\Controllers;

class Home extends BaseController
{

	public function index(){

		return view('calculator');

	}

	public function calculate() {

		// Create two variables to use for checking button inputs
		$numberRelated = "0123456789.";
		$operations = "+-x/";

		$input_data = $this->request->getJSON(true);


		$num1 = $input_data["num1"];
		$num2 = $input_data["num2"];
		$display = $input_data["display"];
		$operationVal = $input_data["operation"];
		$inputVal = $input_data["input"];
		$firstNum = $input_data["firstNum"];
		$displayHold = "";

		// check to see if the display is undefined in order to handle it like a 0 (default in my logic)
		if ($input_data["display"] === "UNDEF") {
			$input_data["display"] = "0";
			$display = $input_data["display"];
		}


		// Check if the clicked button was a number or decimal
		if (strpos($numberRelated, $inputVal) !== false) {
			// If it was a decimal and the first number is empty add a "0.1"
			if ($display === "0" and $inputVal === ".") {
				// If num1 needs to be updated and is blank then add "0.1"
				if ($firstNum and $num1 === "") {
					$input_data["num1"] = "0.";
				}
				// If num2 needs to be updated and is blank then add "0.1"
				elseif (!$firstNum and $num2 === "") {
					$input_data["num2"] = "0.";
				}
			}
			// If num1 needs to be updated add the value of the button
			elseif ($firstNum) {
				$input_data["num1"] .= $inputVal;
			}
			// If num2 needs to be updated add the value of the button
			else {
				$input_data["num2"] .= $inputVal;
			}
		}
		// Check to see if an operation was punched in
		elseif (strpos($operations, $inputVal) !== false and $firstNum) {
			// If no conetent set operation to "0" so that display stays up like default
			if ($num1 === "") {
				$displayHold = "0";
			}
			elseif (strpos($display, $inputVal) === false ) {
				$input_data["operation"] = " " . $inputVal . " ";
				$input_data["firstNum"] = false;
			}
		}
		// Check to see if clear was pressed
		elseif ($inputVal === "C") {
			$input_data["num1"] = "";
			$input_data["num2"] = "";
			$input_data["firstNum"] = true;
			$input_data["operation"] = "";
			$input_data["input"] = "";
			$displayHold = "0";
		}
		// Check to see if the user clicked on equals
		elseif (($inputVal === "=") and ($num2 !== "") and ($operationVal !== "")) {
			$first = (double)$num1;
			$second = (double)$num2;
			// Multiply numbers
			if ($operationVal === " x ") {
				$input_data["num1"] = $first * $second;
			}
			// Divide Numbers
			elseif ($operationVal === " / ") {
				if ($second === 0.0 or $second === 0) {
					$input_data["num1"] = "UNDEF";
					$input_data["firstNum"] = true;
				}
				else {
					$input_data["num1"] = $first / $second;
				}
			}
			// Subtract Numbers
			elseif ($operationVal === " - ") {
				$input_data["num1"] = $first - $second;
			}
			// Add numbers
			elseif ($operationVal === " + ") {
				$input_data["num1"] = $first + $second;
			}
			// Set other key value pairs to defaults so they do not mess with display string
			$input_data["num2"] = "";
			$input_data["firstNum"] = true;
			$input_data["operation"] = "";
			$input_data["input"] = "";
		}
		// Check for "%" or "+/-"
		elseif ($inputVal === "%" or $inputVal === "+/-") {
			if ($inputVal === "%") {
				if ($firstNum) {
					$input_data["num1"] = (double)$num1 * 0.01;
				}
				else {
					$input_data["num2"] = (double)$num2 * 0.01;
				}
			}
			else {
				if ($firstNum) {
					$input_data["num1"] = (double)$num1 * -1;
				}
				else {
					$input_data["num2"] = (double)$num2 * -1;
				}
			}
		}

		// Create correct display string for javascript to display once the JSON is returned
		$input_data["display"] = $input_data["num1"] . $input_data["operation"] . $input_data["num2"] . $displayHold;

		// Make $json variable of type JSON to be sent to both the database and client
		$json = $this->response->setJSON($input_data);

		// Set session json object to $json data that has been modified based on calculator logic
		$this->session->set('data', $json);
		// Pass back the $json variable of type json to the javascript event handler
		return $json;

	}

	public function getSessionData() {

		$input_data = $this->request->getJSON(true);


		if ($this->session->has('data')) {
			//$input_data["display"] = "Modified";
			return $this->session->get('data');
		}
		else {
			return $this->response->setJSON($input_data);;
		}


	}

}
