<?php namespace App\Controllers;

class InfoPage extends BaseController {

	/*
	 * Description:
	 * 		The infoPage controller Index method checks to see if there is a user logged in through
	 * 		Aauth and then decides what links in the nav should be available before adding
	 * 		them to the to rest of the page specific information to be loaded into the view
	 * @param: none
	 * @return: infoContent page with setup data
	 */
	public function index() {

		if ($this->aauth->isLoggedIn()) {
			$linksArray = [
				'Public Gallery' => 'Gallery',
				'Personal Gallery' => 'PersonalGallery',
				'Logout' => '/Account/Logout'
			];
		}
		else {
			$linksArray = [
				'Public Gallery' => 'Gallery',
				'Login' => '/Account/Login',
			];
		}

		// Data to be passed into views
		$data = [
			'title' => 'Info Page',
			'mainHeading' => 'Info Page',
			'links' => $linksArray
		];

		return view('/Photo-Synthesis/infoContent.php', $data);

	}


}
