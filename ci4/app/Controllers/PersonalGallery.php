<?php namespace App\Controllers;

class PersonalGallery extends BaseController {

	/*
	 * Summary:
	 * 		The PersonalGallery controller Index method checks to see if there is a user logged in
	 * 		through Aauth and then decides what links in the nav should be available before adding
	 * 		them to the to rest of the page specific information to be loaded into the view using
	 * 		the $data variable. In this case there is not a user logged in they are redirected to
	 * 		to the login page.
	 * 		Additionally Codeigniter's query builder library is utilized to connect to the database
	 * 		table containing the user post data that was submitted by the logged in user.
	 * 		The query result is then passed through the $data variable to be used in the view.
	 * @param: none
	 * @return: personalGallery page with setup data stored in $data
	 */
	public function index() {

		$queryResultsArray = array();

		// If the user is logged into the website the page can be displayed and the nav links are set
		if ($this->aauth->isLoggedIn()) {
			$linksArray = [
				'Info Page' => 'InfoPage',
				'Public Gallery' => 'Gallery',
				'Logout' => '/Account/Logout'
			];
		}
		// IF the user is not logged in then redirect to login before allowing access to a gallery
		else {
			$linksArray = [
				'Info Page' => 'InfoPage',
				'Public Gallery' => 'Gallery'
			];
			return redirect()->to(base_url('Account/login'));;
		}

		// Get UserId that can be used to select the correct user array for user information
		$userID = $this->aauth->getUserId();
		$userArray = $this->aauth->getUser($userID);

		// Connect to the database table for 'user_posts'
		$builder = $this->db->table('user_posts');

		// Query Database for data associated with the user
		$builder->select('*')->where('username', $userArray['username'])->orderBy('id', 'desc');
		$queryResults = $builder->get();
		$queryResultsArray = $queryResults->getResult('array');

		// Data to be passed into views (will update more key => value pairs later
		$data = [
			'title' => 'Personal Gallery',
			'mainHeading' => 'Personal Gallery',
			'styleHref' => '/style/galleryStyle.css',
			'links' => $linksArray,
			'userUploadData' => $queryResultsArray
		];

		return view('Photo-Synthesis/personalGallery.php', $data);

	}

	/*
	 * Summary The PersonalGallery controller connects to the correct database table and
	 * 		then gets the post data including the image file uploaded. It then stores the image
	 * 		on the server in a non public location for security. The user information necessary
	 * 		for the post information is gathered and the database table is updated. Lastly the
	 * 		the page is redirected to the personal gallery page which is refreshed to include the
	 * 		new post
	 * @param: none
	 * @return: redirect to the user's page
	 */
	public function addPhoto() {

		$builder = $this->db->table('user_posts');


		$validated = $this->validate([
			'picTitle' => 'required',
			'file' => 'uploaded[file]'
		]);

		if ($validated) {

			$postData = $this->request->getPost();
			$uploadedImage = $this->request->getFile('file');

			// Get UserId and select the correct user array for user information from the user table
			$userID = $this->aauth->getUserId();
			$userArray = $this->aauth->getUser($userID);

			// Get the uploaded image and store it into the writable/uploads/userImages folder with a new unique name
			if ($uploadedImage) {
				$newImageNameUniqueID = (string)time();
				$newImageName = $newImageNameUniqueID . $uploadedImage->getName();
				$uploadedImage->store('userImages', $newImageName);
				$imageMimeType = $uploadedImage->getClientMimeType();
			}
			else {
				$newImageName = 'Place Holder';
				$imageMimeType = 'Place Holder';
			}

			// Set $public_flag to N for default of private then change to Y only if user checked public box
			$public_flag = 'N';
			if ($postData['publicFlag']) {
				$public_flag = 'Y';
			}

			// Create associative array of data for the image/content records that will go to database
			$picData = [
				'username' => $userArray['username'],
				'pic_title' => $postData['picTitle'],
				'pic_description' => $postData['picDescription'],
				'file_name' => $newImageName,
				'mime_type' => $imageMimeType,
				'public_flag' => $public_flag
			];

			// Insert the users post into the database
			$builder->insert($picData);

		}

		return redirect()->to(base_url('public/index.php/PersonalGallery'));

	}

}
