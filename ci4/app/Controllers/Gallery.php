<?php namespace App\Controllers;


class Gallery extends BaseController {

	/*
	 * Summary:
	 *		The Gallery controller Index method checks to see if there is a user logged in through
	 * 		Aauth and then decides what links in the nav should be available before adding
	 * 		them to the to rest of the page specific information to be loaded into the view using
	 * 		the $data variable.
	 * 		Additionally Codeigniter's query builder library is utilized to connect to the database
	 * 		table containing the user post data that is marked for public access. The query result
	 * 		is then passed through the $data variable to be used in the view.
	 * @param: none
	 * @return: publicGallery page with setup data stored in $data
	 */
	public function index() {

		if ($this->aauth->isLoggedIn()) {
			$linksArray = [
				'Info Page' => 'InfoPage',
				'Personal Gallery' => 'PersonalGallery',
				'Logout' => '/Account/Logout'
			];
		}
		else {
			$linksArray = [
				'Info Page' => 'InfoPage',
				'Login' => '/Account/Login'
			];
		}

		// Create $builder variable with connection to 'user_posts' table in the default database connectino
		$builder = $this->db->table('user_posts');

		// Query database, get the result and then turn the results into an array
		$builder->select('*')
				->where('public_flag', 'Y')
				->orderBy('id', 'desc');
		$queryResults = $builder->get();
		$queryResultsArray = $queryResults->getResult('array');

		// Data to be passed into views (will update more key => value pairs later
		$data = [
			'title' => 'Public Gallery',
			'mainHeading' => 'Public Gallery',
			'styleHref' => '/style/galleryStyle.css',
			'links' => $linksArray,
			'userUploadData' => $queryResultsArray
		];

		return view('/Photo-Synthesis/publicGallery.php', $data);

	}

}
