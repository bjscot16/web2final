<?php namespace App\Controllers;

class Login extends BaseController {

	public function index() {

		// Data to be passed into views (will update more key => value pairs later
		$data = [
			'title' => 'Login',
			'mainHeading' => 'Sign in or Create an Account',
			'src' => '/script/login.js',
			'styleHref' => '/style/loginStyle.css'
		];

		echo view('/Photo-Synthesis/header.php', $data);
		echo view('/Photo-Synthesis/login.php', $data);
		echo view('/Photo-Synthesis/footer.php', $data);
	}

	public function login() {

		$this->form_validation->set_rules('user', 'Username', 'trim|required');
		$this->form_validation->set_rules('pass', 'Password', 'trim|required');



	}

}
