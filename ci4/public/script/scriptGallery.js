$(document).ready(function () {

    console.log("Gallery Script Loaded");

    var imageCount = imageArray.length;
    for (i = 0; i < imageCount; i++) {
        let endPos = imageArray[i]['file_name'].indexOf('.');
        let idName = imageArray[i]['file_name'].substring(0, endPos)
        let selector = "#" + idName;

        $(selector).attr("src", "/GetImage/image/" + imageArray[i]['file_name']);

        /*
        $.ajax({
            type: "POST",
            url: "GetImage/image/" + imageArray[i]['file_name'],
            //contentType: "JSON",
            //data: JSON.stringify(imageArray[i]),
            success: function (data) {
                console.log("Image Request for " + selector + " Returned");
                $(selector).attr("src", data);
            }
        });
        */
    }

});